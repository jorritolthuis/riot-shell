# RIOT shell test

This project belongs in `RIOT/examples/`, and demonstrates a simple shell program that can do some IO.

Compiles by default for Openmote-b, but can be overwritten by setting the `BOARD` environment variable.

Run using

```
$ make
$ sudo make flash
$ sudo make term
```
