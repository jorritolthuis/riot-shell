# Set the name of your application:
APPLICATION = shell-test
 
# If no BOARD is found in the environment, use this default:
BOARD ?= openmote-b
 
# This has to be the absolute path to the RIOT base directory:
RIOTBASE ?= $(CURDIR)/../../../RIOT

USEMODULE += shell
USEMODULE += shell_commands

include $(RIOTBASE)/Makefile.include
