/**
 * @author Jorrit Olthuis
 * @date Jan 18, 2022
 *
 * A simple test for the capabilities of the RIOT OS shell.
 */

#include <stdio.h>
#include <stdlib.h>

#include "shell.h"

int _my_command(int, char**);

int _my_command(int argc, char** argv)
{
	printf("Called command with %d arguments\n", argc);

	if (argc > 0) {
		for (int i=0; i<argc; ++i) {
			puts(argv[i]);
		}
	}

	puts("Hello world\n\nPlease enter some text:\n");

	char* str = malloc(100);
	gets(str);
	puts("\n");
	puts(str);
	
	free(str);

	return 0;
}
//SHELL_COMMAND(test_shell, "Run this command to test the capabilities of the RIOT shell", _my_command);
const shell_command_t test_shell = {.name = "test_shell", 
									.desc = "Run this command to test the capabilities of the RIOT shell", 
									.handler = _my_command};
const shell_command_t end = {	.name = NULL, 
								.desc = NULL, 
								.handler = NULL};

const shell_command_t shell_commands[] = {test_shell, end};

int main(void)
{
	char* line_buffer = malloc(100);

	shell_run_forever(shell_commands, line_buffer, 100);

	free(line_buffer);
	return 0;
}
